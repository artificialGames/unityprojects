﻿using UnityEngine;
using System.Collections;

public class SuperJumpPowerUp : MonoBehaviour
{
	// TODO 1 - Atributo público de tipo float que representará la duración del power up
	/// <summary>
	/// Atributo que indica el tiempo que durará el powerUp
	/// </summary>
	public float m_duration;
	/// <summary>
	/// Atributo que indica la altura máxima de salto que alcanzará
	/// el jugador cuando el power up esté activo
	/// </summary>
	public float m_SuperJumpHeight = 4.0f;

	/// <summary>
	/// Cuando el jugador toca el ítem, este debe otorgar la habilidad de super-salto al jugador
	/// durante un tiempo determinado
	/// </summary>
	/// <param name="other">
	/// Objeto que chica contra el item <see cref="Collider"/>
	/// </param>
	void OnTriggerEnter(Collider other)
	{
		// TODO 2 - Si el objeto que entra en mi trigger tiene el tag player

		if (other.tag == "")
		{
			// TODO 3 - Le envío un mensaje "SetJumpHeight" con la altura que tengo configurada para el super-salto

			// TODO 4 - Desactivo el renderer y el collider de mi gameObject
			// Pista: atributo "enabled"
	
			// TODO Refactor 1 - Iniciar el timer del GUIManager (método StartPowerUpTimer)


			// TODO Refactor 2 - Obtener el componente TrailRenderer del jugador y activarlo
			
		}

		//yield return new WaitForSeconds(m_duration);

		// TODO 5 - Envío un mensaje recuperando la altura del salto anterior (por defecto, 6)

		// TODO Refactor 2 - Obtener el componente TrailRenderer del jugador y desactivarlo

	}
}
