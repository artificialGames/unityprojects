﻿using UnityEngine;
using System.Collections;

public class DrawTrajectory : MonoBehaviour {

    public Transform _start;
    public Transform _end;
	// Use this for initialization
	void Start ()
    {
        LineRenderer lr = GetComponent<LineRenderer>();
        lr.SetPosition(0, _start.position);
        lr.SetPosition(1, _end.position);
    }
	
}
