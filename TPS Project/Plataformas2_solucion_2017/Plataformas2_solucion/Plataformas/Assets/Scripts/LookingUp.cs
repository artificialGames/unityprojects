﻿using UnityEngine;
using System.Collections;

public class LookingUp : MonoBehaviour {

	/// <summary>
	/// En la función LateUpdate simplemente asignamos los ejes del GameObject para que
	/// siempre este mirando hacia arriba
	/// </summary>
	void LateUpdate()
	{
		// TODO 1 - El transform.up del gameObject tendrá que ser el Up del mundo
		transform.up = Vector3.up;
		// TODO 2 - El transform.right del gameObject tendrá que ser el right que tenga su padre
		transform.right = transform.parent.right;
		// TODO 3 - El transform.forward del gameObject tendrá que ser el forward que tenga su padre
		transform.forward = transform.parent.forward;
	}
}
