﻿using UnityEngine;

public class CutsceneLauncher : MonoBehaviour {

	/// <summary>
	/// Referencia al gameObject cutsceneManager
	/// Necesaria para poder hacer el "play" de la animación, y que por lo tanto
	/// comience a funcionar la cutscene
	/// </summary>
	public CutsceneScript m_CutSceneScript = null;
	
	
	/// <summary>
	/// En cuanto el jugador toque el trigger, empezará la Cutscene
	/// </summary>
	/// <param name="other">
	/// El gameobject que entra en el trigger <see cref="Collider"/>
	/// </param>
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
		{
			// TODO 1 - Si existe m_CutSceneScript lanzamos la cutscene
			// Se presupone que el guión de tiempos de la cutscene estará dentro 
			// del método invocado
            if(m_CutSceneScript != null)
            {
                m_CutSceneScript.PlayCutscene();
            }
            Destroy(gameObject);
		}	
	}
}
