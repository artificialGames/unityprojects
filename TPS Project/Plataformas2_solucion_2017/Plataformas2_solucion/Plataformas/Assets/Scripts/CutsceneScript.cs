﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class CutsceneScript : MonoBehaviour {

	/// <summary>
	/// Plataforma interruptor de la cutscene
	/// </summary>
	public GameObject m_SwitchPlatform = null;
	
	/// <summary>
	/// Cámaras involucradas en el transcurso de la cutscene
	/// </summary>
	public GameObject m_SwitchCamera = null;
	public GameObject m_FenceCamera = null;
	
	
	/// <summary>
	/// La barrera de seguridad que desactivaremos :)
	/// </summary>
	public GameObject m_Fence = null;
	
	/// <summary>
	/// Referencia al player
	/// </summary>
	private GameObject m_Player = null;
	
	/// <summary>
	/// Main camera
	/// </summary>
	private Camera m_MainCamera = null;

	/// <summary>
	/// Use for initialization
	/// </summary>
	void Start()
	{
		m_Player = GameObject.FindGameObjectWithTag("Player");
		
		m_MainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
	}
	
	
	/// <summary>
	/// Función que inicia la cutscene. Desactiva el control del player
	/// y hace play de la animación de la plataforma-interruptor
	/// </summary>
	public void StartCutscene () {
		
		Debug.Log("StartCutScene()");

        // TODO 1 - Desactivar el control del player desactivando su ThirdPersonController (usar GetComponent y enable).
        // Esto hará que no pueda ser controlado mientras dure la cutscene
        m_Player.GetComponent<ThirdPersonCharacter>().enabled = false;
        m_Player.GetComponent<Rigidbody>().velocity = Vector3.zero;
        m_Player.GetComponent<Animator>().enabled = false;
        m_Player.GetComponent<ThirdPersonUserControl>().enabled = false;
        // TODO 2 - Activar la cámara del interruptor, y desactivar las otras 2
        m_SwitchCamera.SetActive(true);
        m_FenceCamera.SetActive(false);
        m_MainCamera.gameObject.SetActive(false);
        // TODO 3 - Hacer play de la animación de la plataforma del interruptor
        m_SwitchPlatform.GetComponent<Animation>().Play();
	}
	
	/// <summary>
	/// Simplemente realiza un cambio de cámara
	/// </summary>
	public void ChangeCamera1 () {
		
		Debug.Log("ChangeCamera1()");
		
		m_SwitchCamera.SetActive(false);
		m_FenceCamera.SetActive(true);
	
	}

	/// <summary>
	/// Función que desactiva la barrera de fuerza
	/// </summary>
	public void DeactivateBarrier()
	{
		Debug.Log("DeactivateBarrier()");

        // TODO 4 - Desactivar la barrera
        m_Fence.SetActive(false);
	}
	
	/// <summary>
	/// Función que re-activa la cámara del jugador y devuelve el control del mismo
	/// </summary>
	public void ChangeCamera2()
	{
		Debug.Log("ChangeCamera2()");

        // TODO 5 - Recuperar el control del player, y activar la main camera desactivando las demás
        m_Player.GetComponent<ThirdPersonCharacter>().enabled = true;
        m_Player.GetComponent<Rigidbody>().velocity = Vector3.zero;
        m_Player.GetComponent<Animator>().enabled = true;
        m_Player.GetComponent<ThirdPersonUserControl>().enabled = true;
        // TODO 2 - Activar la cámara del interruptor, y desactivar las otras 2
        m_FenceCamera.SetActive(false);
        m_MainCamera.gameObject.SetActive(true);
    }

	/// <summary>
	/// Reproduce la cutscene. Invoca a una corrutina que hace el trabajo sucio
	/// </summary>
	public void PlayCutscene()
	{
		StartCoroutine(CutsceneCoroutine());
	}


	/// <summary>
	/// Corrutina encargada de reproducir la cutscene con las pausas correspondientes
	/// </summary>
	/// <returns></returns>
	public IEnumerator CutsceneCoroutine()
	{
        // TODO 6 - Lanzar cada una de las funciones de la cutscene y después 
        // esperar los segundos necesarios (yield return new WaitForSeconds)
        // Guion: 
        // Tiempo     Función
        // 0:00   --> StartCutscene
        StartCutscene();
        // 3:00   --> ChangeCamera1
        yield return new WaitForSeconds(3f);
        ChangeCamera1();
        // 4:15   --> DeactivateBarrier
        yield return new WaitForSeconds(1.15f);
        DeactivateBarrier();
        // 5:40   --> ChangeCamera2
        yield return new WaitForSeconds(1.25f);
        ChangeCamera2();

		yield break;

	}
}
