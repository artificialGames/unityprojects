﻿using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class FenceTextureOffset : MonoBehaviour {

	/// <summary>
	/// Atributo público que controla la velocidad de movimiento de la textura
	/// </summary>
	public float scrollSpeed = 0.25f;
	
	/// <summary>
	/// En la función update se modifica el offset de la textura principal del objeto
	/// </summary>
	void Update () {

        // TODO 1 - Obtener el incremento del offset multiplicando la velocidad de scroll por el tiempo (sólo crecerá)
        float offset = scrollSpeed * Time.deltaTime;
        // TODO 2- Modificar el mainTextureOffset del renderer.material.
        // Habrá que pasarle un Vector2, cuyas componentes x e y, serán el offset antes calculado
        GetComponent<Renderer>().material.mainTextureOffset += Vector2.down * offset;
	}
}
