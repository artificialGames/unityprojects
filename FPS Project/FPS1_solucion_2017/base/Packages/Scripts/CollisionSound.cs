using UnityEngine;
using System.Collections;

// Esta clase se encarga de reproducir un sonido cuando el GameObject
// padre colisiona contra cualquier objeto
public class CollisionSound : MonoBehaviour
{	
	/// <summary>
	/// AudioClip público. Será el sonido que se reproducirá cuando el GameObject colisione
	/// </summary>
	// ## TO-DO 1 - Añadir una variable pública de tipo AudioClip. ##
	public AudioClip m_CollisionSound;
	
	// Esta función se llama cada vez que el objeto colisiona contra algún objeto
    // ## TO-DO 2 - Añadir la función de la API que se lanza cada vez que el GameObject colisiona. Pista: OnCollisi...
    void xxxx()
	{    
		// ## TO- DO 3 - En caso de que haya sonido, reproducirlo una única vez. Pista: audio.PlayOne...
    }
}
