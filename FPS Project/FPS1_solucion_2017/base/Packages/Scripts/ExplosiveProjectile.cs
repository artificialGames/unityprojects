using UnityEngine;
using System.Collections;

/// <summary>
/// La presente clase implementa un tipo de proyectil explosivo
/// Cuando el proyectil choca contra alguna superficie, desaparece
/// se instancian unas partículas de explosion, y se reproduce un sonido 3D
/// </summary>
public class ExplosiveProjectile : MonoBehaviour
{	
	#region Exposed fields
	
	/// <summary>
	/// Partículas que se instancian cuando el proyectil explota
	/// </summary>
	public GameObject m_Explosion;
	
	/// <summary>
	/// Radio de la explosión
	/// </summary>
	public float m_ExplosionRadius = 5.0f;
	
	/// <summary>
	/// Potencia de la explosión
	/// </summary>
	public float m_ExplosionPower = 10.0f;
	
	#endregion
	
	// Cuando el proyectil choca contra alguna superficie se produce la explosión
	void OnCollisionEnter(Collision collision)
	{
        // Creamos una instancia de las partículas de la explosión
        ContactPoint explosionSource = collision.contacts[0];
        Quaternion explosionRotation = Quaternion.FromToRotation(Vector3.up, explosionSource.normal);
        Instantiate(m_Explosion, explosionSource.point, explosionRotation);
		
		// Comprobamos qué GameObject estaban dentro del radio de la explosión
		// Si son de tipo Rigidbody, les aplicamos una fuerza
		Collider[] colliders = Physics.OverlapSphere(transform.position, m_ExplosionRadius);
        foreach (Collider hit in colliders) 
        {
            if (hit.rigidbody)
                hit.rigidbody.AddExplosionForce(m_ExplosionPower, transform.position, m_ExplosionRadius, 3.0F);
        }

        // Destruimos el GameObject
        Destroy(gameObject);		
	}
}
