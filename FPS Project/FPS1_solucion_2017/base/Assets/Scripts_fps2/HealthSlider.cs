﻿using UnityEngine;
using System.Collections;

public class HealthSlider : MonoBehaviour {


    public Health m_health;

    private UnityEngine.UI.Slider m_slider;

	// Use this for initialization
	void Start () {

        //TO-DO 1 Cargat HealthSlider y configurarlo
        m_slider = GetComponent<UnityEngine.UI.Slider>();
        if(m_slider != null)
        {
            m_slider.minValue = 0f;
            m_slider.maxValue = m_health.m_health;
            m_slider.value = m_health.m_health;
        } 
    }
	
	// Update is called once per frame
	void Update ()
    {
        m_slider.value = m_health.m_health;
    }
}
