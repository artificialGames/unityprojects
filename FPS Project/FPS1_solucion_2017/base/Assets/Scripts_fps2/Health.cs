﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

    /// <summary>
    /// 
    /// </summary>
    /// // ## TO-DO 1 la salud inicial de la entidad.
    public float m_health;
    // Use this for initialization
    private float m_currenthealth;

    void Start () {
        m_currenthealth = m_health;
    }

    public void Respawn()
    {
        m_health = m_currenthealth;
    }


    /// <summary>
    /// Mensaje que aplica el daño y lanza el mensaje OnDeath cuando la salud es menor que 0.
    /// </summary>
    /// <param name="amount"></param>
    public void Damage(float amount)
    {
        m_health -= amount;
        ///  // ## TO-DO 2 si la salud inicial es menor que 0 enviar mensaje void OnDeath() por si a alguien le interesa..
        if(m_health <= 0)
        {
            SendMessage("OnDeath", SendMessageOptions.DontRequireReceiver);
        }
    }

}
