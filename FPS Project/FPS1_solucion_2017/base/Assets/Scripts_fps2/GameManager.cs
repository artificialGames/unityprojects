using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/// <summary>
/// Esta clase se encarga de guardar información relevante sobre la lógica
/// y progreso del juego. Deberá estar siempre disponble, por lo que el usuario
/// se tiene que asegurar de que este componente nunca sea eliminado de la escena
/// </summary>
public class GameManager : MonoBehaviour {
	
	#region Helpers
	
	/// <summary>
	/// Enumerado que almacena los posibles colores de las puertas
	/// Será útil para la activación/desacivación de las mismas
	/// </summary>
	public enum DoorColor
	{
		GREEN = 0,
		BLUE = 1,
		YELLOW = 2
	}
	
	#endregion
	
	#region Exposed Fields
	
	/// <summary>
	/// Punto de spawn inicial
	/// </summary>
	public Transform m_InitialSpawnPoint;
	
	/// <summary>
	/// ¿Quién es el jugador?
	/// </summary>
	public GameObject m_Player;
	
	/// <summary>
	/// Referencia a la puerta verde
	/// </summary>
	public GameObject m_GreenDoor;

	/// <summary>
	/// Referencia a la puerta azul
	/// </summary>
	public GameObject m_BlueDoor;
	
	/// <summary>
	/// Referencia a la puerta amarilla
	/// </summary>
	public GameObject m_YellowDoor;
	
	/// <summary>
	/// Este atributo controla si las puertas estarán activadas o no
	/// cuando el jugador comience el juego
	/// </summary>
	public bool m_Cheater = false;
	
	#endregion
	
	#region Non-Exposed Fields
	
	/// <summary>
	/// Actual punto de spawn. 
	/// Durante el juego el punto de spawn puede variar (en función de los 
	/// niveles que hayamos desbloqueado)
	/// </summary>
	private Transform m_CurrentSpawnPoint;
	
	/// <summary>
	/// Diccionario que contiene cada puerta asociada a su color
	/// Se poblará en el Awake
	/// </summary>
	private Dictionary<DoorColor, GameObject> m_Doors = new Dictionary<DoorColor, GameObject>();
	
	/// <summary>
	/// En esta lista se almacenan los niveles ya cargados
	/// </summary>
	private List<DoorColor> m_LevelsLoaded = new List<DoorColor>();
	
	/// <summary>
	/// Este diccionario relaciona un color con el nombre del nivel que le corresponde
	/// </summary>
	private Dictionary<DoorColor, string> m_LevelNames = new Dictionary<DoorColor, string>();


	#endregion
	
	void Awake()
	{
		if(!m_InitialSpawnPoint)
			Debug.LogWarning("No se ha asignado un punto de spawn inicial");
		
		// Al principio, el punto incial será el punto actual de spawn
		m_CurrentSpawnPoint = m_InitialSpawnPoint;
		
		// Rellenamos el diccionario con las referencias necesarias
		m_Doors.Add(DoorColor.GREEN, m_GreenDoor);
		m_Doors.Add(DoorColor.BLUE, m_BlueDoor);
		m_Doors.Add(DoorColor.YELLOW, m_YellowDoor);
		
		// Rellenamos el diccionario con los nombres de los niveles
		m_LevelNames.Add(DoorColor.GREEN, "greenworld");
		m_LevelNames.Add(DoorColor.BLUE, "blueworld");
		m_LevelNames.Add(DoorColor.YELLOW, "yellowworld");


    }

	/// <summary>
	/// Inicialización del jugador. Se recoloca en el escenario
	/// </summary>
	void Start () {
	
		// Si juego legalmente, desactivo las puertas
		if (!m_Cheater)
		{
			ActivateDoor(DoorColor.BLUE, false);
			ActivateDoor(DoorColor.YELLOW, false);
		}
		RespawnPlayer();
	}
	
	/// <summary>
	/// Desde fuera pueden configurar el punto de spawn
	/// </summary>
	/// <param name="current">
	/// Nuevo punto de spawn <see cref="Transform"/>
	/// </param>
	public void SetCurrentSpawnPoint(Transform current)
	{
		m_CurrentSpawnPoint = current;	
	}
	
	/// <summary>
	/// Desde fuera nos pueden pedir el punto de spawn
	/// </summary>
	/// <returns>
	/// Actual punto de spawn <see cref="Transform"/>
	/// </returns>
	public Transform GetCurrentSpawnPoint()
	{
		return m_CurrentSpawnPoint;
	}
	
	/// <summary>
	/// Esta función setea la posición del player, haciéndola coincidir
	/// con la posición del punto de spawn actual
	/// </summary>
	public void RespawnPlayer()
	{
		// Colocamos al player en el punto de spawn actual
		m_Player.transform.position = m_CurrentSpawnPoint.position;
        m_Player.SendMessage("Respawn");
    }
	
	/// <summary>
	/// Esta función activa/desactiva una puerta con un tipo dado
	/// </summary>
	/// <param name="doorColor">
	/// Color de la puerta que se quiere desactivar <see cref="DoorColor"/>
	/// </param>
	/// <param name="value">
	/// True, activa la puerta, false la desactiva <see cref="System.Boolean"/>
	/// </param>
	public void ActivateDoor(DoorColor doorColor, bool value)
	{
		// Si la puerta existe, la intentamos activar
		if (m_Doors.ContainsKey(doorColor))
		{
			GameObject doorToActivate = m_Doors[doorColor];
			if (doorToActivate)
			{
				// Si la referencia está bien configurada, es posible activarla
				doorToActivate.SetActive(value);
			}
			else
				Debug.LogWarning("La puerta que se está intentando activar, no se ha asignado en el GameManager");
		}
	}
	
	/// <summary>
	/// Función que carga un nivel determinado a partir del tipo
	/// </summary>
	/// <param name="doorColor">
	/// Color del nivel que se quiere cargar <see cref="DoorColor"/>
	/// </param>
	public void TriggerLoadAdditive(DoorColor doorColor)
	{
		// Si el nivel no está cargado, se carga
		if (!m_LevelsLoaded.Contains(doorColor))
		{
			SceneManager.LoadScene(m_LevelNames[doorColor],LoadSceneMode.Additive);
			// Es muy importante añadir el nivel como cargado
			m_LevelsLoaded.Add(doorColor);
		}else
			Debug.LogWarning("El nivel ["+m_LevelNames[doorColor]+"] ya se ha cargado. No se va a volver a realizar la carga");
	}
}
