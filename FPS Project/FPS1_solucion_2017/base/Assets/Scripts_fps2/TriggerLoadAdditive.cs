using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

/// <summary>
/// Este componente se encarga de detectar la entrada de un GameObject
/// en un trigger, y de realizar una carga aditiva de un nivel indicado anteriormente
/// </summary>
public class TriggerLoadAdditive : MonoBehaviour {

    #region Exposed Fields

    // TODO 1 - Añadir string público que será el nombre del nivel a cargar

    public bool m_old = false;


    /// <summary>
    /// Color del nivel que activa este trigger
    /// </summary>
    public GameManager.DoorColor m_ColorToLoad;
	
	#endregion
	
	#region Non-exposed Fields
	
	/// <summary>
	/// La comprobación del tipo de gameobject que entra en el trigger se hace por tag
	/// El valor del tag que nos interesa se guarda en esta variable
	/// </summary>
	private string m_PlayerTag = "Player";
    #endregion



    /// <summary>
    /// Detecta cuándo un GameObject entra en el trigger al cual está asignado este componente.
    /// En nuestro caso, realizamos la carga aditiva del nivel indicado en el atributo
    /// público "LevelToLoadName"
    /// </summary>
    void OnTriggerEnter(Collider other)
	{

        // TODO 2 - Hacer función que cargue un nivel de forma aditiva.
        // Será necesario pasarle el gameObject que ha colisionado con el trigger

        //else
            //LoadLevelFromGameManager(other.gameObject);
        // TODO 3 (Refactor) - Bloquear el TODO-2 y llamar a LoadLevelFromGameManager(other.gameObject);
    }


    #region Refactor


    // TODO Refactor 1 - Añadir una variable privada de tipo GameObject para almacenar una referencia al GameManager


    // TODO Refactor 2 - Añadir funcion Start que resuelva la referencia del GameManager

    // TODO Refactor 3 - Crear una función LoadLevelFromGameManager que reciba un GameObejct y se encargue de
    // cargar un nivel enviando un mensaje "TriggerLoadAdditive" al GameManager
    // Como parámetro del mensaje habrá que enviar el color del nivel que debe cargar. Pista: atributo m_ColorToLoad


    // TODO Refactor 4 - En la función OnTriggerEnter, llamar a LoadLevelFromGameManager en vez de a la función
    // que se llamaba antes
    //_-------------
    // Para esto hay que modificar código de la sección superior


    #endregion
}
