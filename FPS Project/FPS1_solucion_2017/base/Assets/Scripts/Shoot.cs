using UnityEngine;
using System.Collections;

// Contiene la declaración de la clase Shoot, encargada de la mecánica de disparo.
// Permite dos formas de disparo exclusivas:
//      - Proyectiles
//      - Raycast
public class Shoot : MonoBehaviour
{
	const float LENGHT_SHOOT_LINE = 2f;
    #region Exposed fields

    /// <summary>
    /// Proyectil a disparar. Si no está asignado, la mecánica de disparo utilizará
    /// Raycast para calcular los puntos de impacto
    /// </summary>
    /// 
    //  ## TO-DO 1 - Declarar la variable pública. Tipo: Rigidbody ## 
	public Rigidbody m_Projectile;
	
	/// <summary>
	/// Velocidad inicial del proyectil que se dispara
	/// </summary>
	// ## TO-DO 2- Declarar la variable pública. Tipo: float ## 
	public float m_InitialVelocity = 50;
	
	/// <summary>
	/// Punto desde el que se dispara el proyectil
	/// </summary>
	// ## TO-DO 3- Declarar la variable pública. Tipo: Transform ## 
	public Transform m_ShootPoint;
	
	/// <summary>
	/// Tiempo transcurrido entre disparos
	/// </summary>
	[System.NonSerialized]
	public float m_TimeBetweenShots = 0.25f;
	
	/// <summary>
	/// Booleano para indicar si el arma es automática
	/// </summary>
	public bool m_IsAutomatic = false;

    /// <summary>
    /// Particulas que saltan cuando un arma sin proyectil acierta en algo.
    /// </summary>
    public GameObject m_Sparkles;

    /// <summary>
    /// Define el alcance del arma que no utiliza proyectiles.
    /// </summary>
    public float m_ShootRange = 100;

    /// <summary>
    /// Fuerza que aplican los disparos que no usan proyectiles.
    /// </summary>
    public float m_ShootForce = 10;

    /// <summary>
    /// Sonido del arma.
    /// </summary>
    public AudioClip m_ShootAudio;

    #endregion

    #region Non exposed fields
	
	/// <summary>
	/// Tiempo transcurrido desde el último disparo
	/// </summary>
	private float m_TimeSinceLastShot = 0;

    /// <summary>
    /// Indica si estamos disparando (util en modo automático).
    /// </summary>
    private bool m_IsShooting = false;

    #endregion

    #region Monobehaviour Calls
	
	/// <summary>
    /// En el método Update se consultará al Input si se ha pulsado el botón de disparo
	/// </summary>
	void Update () {

        // Será necesario llevar cuenta del tiempo transcurrido

        //  ## TO-DO 6 - Actualizar el contador m_TimeSinceLastShot ## 
        // Para ello, habrá que sumarle el tiempo de ejecución del anterior frame
        m_TimeSinceLastShot += Time.deltaTime;
		
		if (GetFireButton())
		{
            if (CanShoot())
            {
                // ## TO-DO 9 - En función de si hay proyectil o no, usar la función de disparo
                // con proyectil, o la de disparo con rayo ## 
                if (m_Projectile != null)
                    ShootProjectile();
                else
                    ShootRay();
                // ## TO-DO 7 - Reiniciar el contador m_TimeSinceLastShot ## 
                m_TimeSinceLastShot = 0f;
            }

            if (!m_IsShooting)
            {
                m_IsShooting = true;

                // ## TO-DO 11 
                // 1.- Rotar el "barrel" del arma.
                // 2.- Poner sonido de disparo.
                BroadcastMessage("SHOOT", true, SendMessageOptions.DontRequireReceiver);
                AudioSource audio = GetComponent<AudioSource>();
                if (audio)
                    audio.Play();

            }
		}
        else if (m_IsShooting)
        {
            m_IsShooting = false;

            // ## TO-DO 12 
            // 1.- Parar de rotar el "barrel" del arma.
            // 2.- Parar sonido de disparo.
            BroadcastMessage("SHOOT", false, SendMessageOptions.DontRequireReceiver);
            AudioSource audio = GetComponent<AudioSource>();
            if (audio)
                audio.Stop();
        }

    }
	
	// 
    /// <summary>
    /// En esta función comprobamos si el tiempo que ha pasado desde la última vez que disparamos
    /// es suficiente para que nos dejen volver a disparar 
    /// </summary>
    /// <returns>true si puede disparar y falso si no puede.</returns>
	private bool CanShoot()
	{
		//  ## TO-DO 8 - Comprobar si puedo disparar ##
        return m_TimeSinceLastShot >= m_TimeBetweenShots;
	}
	
    /// <summary>
    /// Devuelve si se ha pulsado el botón de disparo
    /// </summary>
    /// <returns>true si puede disparar y falso si no puede.</returns>
	private bool GetFireButton()
	{
        //  ## TO-DO 4 ## 
        // Retornar directamente si se ha detectado la pulsación del botón "Fire1"
        // Pista: Input.GetButtonD...
        if (m_IsAutomatic)
            return (Input.GetButton("Fire1"));
        else
		    return (Input.GetButtonDown("Fire1"));
		//return false;
	}

	void OnDrawGizmos()
	{
		if(m_ShootPoint != null)
		{
			Debug.DrawLine(m_ShootPoint.transform.position,m_ShootPoint.transform.position+m_ShootPoint.transform.forward * LENGHT_SHOOT_LINE);
		}
	}
	
    /// <summary>
    /// Disparamos un proyectil.
    /// </summary>
	private void ShootProjectile()
	{
		// TO-DO 5
		// 1.- Instanciar el proyectil pasado como variable pública de la clase, en la posición y rotación del punto de disparo
		// 1.2.- Guardarse el objeto devuelto en una variable de tipo Rigidbody
		// 2.- Asignar una velocidad inicial en función de m_Velocity. La dirección será la del m_ShootPoint -> Pista: m_ShootPoint.transform.TransformDirection
		// 3.- Ignorar las colisiones entre nuestro proyectil y nosotros mismos
		Rigidbody instantiatedProjectile = Instantiate(m_Projectile, m_ShootPoint.position, m_ShootPoint.rotation) as Rigidbody;
		instantiatedProjectile.velocity = m_ShootPoint.transform.TransformDirection(0,0,m_InitialVelocity);
		Physics.IgnoreCollision(instantiatedProjectile.GetComponent<Collider>(), transform.root.GetComponent<Collider>());
	
		//Debug.Log("¡Pollo!");
	}

    /// <summary>
    /// Disparamos usando un rayo.
    /// </summary>
    private void ShootRay()
    {
        // ## TO-DO 10 - Función que dispara con rayos ## 
        // 1.- Lanzar un rayo utlizando para ello el módulo de física -> pista Physics.Ra...
        // 2.- Aplicar una fuerza en el punto de impacto.
        // 3.- Colocar particulas de chispas en el punto de impacto -> pista Instanciamos pero no nos preocupasmo del destroy porque el asset puede autodestruirse (componente particle animator).
        Vector3 direction = transform.TransformDirection(Vector3.forward);
        RaycastHit hitInfo;
        Ray ray = new Ray(m_ShootPoint.position, direction);
        if (Physics.Raycast(ray, out hitInfo))
        {
            if(hitInfo.rigidbody != null && !hitInfo.rigidbody.isKinematic)
                hitInfo.rigidbody.AddForce(direction*m_ShootForce, ForceMode.Impulse);
            Instantiate(m_Sparkles, hitInfo.point, Quaternion.identity);
        }
    }

    #endregion
}
