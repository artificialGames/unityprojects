using UnityEngine;
using System.Collections;

// Esta clase se encarga de reproducir un sonido cuando el GameObject
// padre colisiona contra cualquier objeto
public class CollisionSound : MonoBehaviour
{	
	/// <summary>
	/// AudioClip pÃºblico. SerÃ¡ el sonido que se reproducirÃ¡ cuando el GameObject colisione
	/// </summary>
	// ## TO-DO 1 - AÃ±adir una variable pÃºblica de tipo AudioClip. ##
	public AudioClip m_CollisionSound;
	
	// Esta funciÃ³n se llama cada vez que el objeto colisiona contra algÃºn objeto
    // ## TO-DO 2 - AÃ±adir la funciÃ³n de la API que se lanza cada vez que el GameObject colisiona. Pista: OnCollisi...
    void OnCollisionEnter(Collision collision)
	{    
		// ## TO- DO 3 - En caso de que haya sonido, reproducirlo una Ãºnica vez. Pista: audio.PlayOne...
		if (m_CollisionSound)
			GetComponent<AudioSource>().PlayOneShot(m_CollisionSound);
    }
}
