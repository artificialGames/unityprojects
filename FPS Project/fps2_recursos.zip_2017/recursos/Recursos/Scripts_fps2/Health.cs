﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

    /// <summary>
    /// 
    /// </summary>
    /// // ## TO-DO 1 la salud inicial de la entidad.

    // Use this for initialization


    void Start () {
        
    }

    /// <summary>
    /// Mensaje que aplica el daño y lanza el mensaje OnDeath cuando la salud es menor que 0.
    /// </summary>
    /// <param name="amount"></param>
    public void Damage(float amount)
    {

        ///  // ## TO-DO 2 si la salud inicial es menor que 0 enviar mensaje void OnDeath() por si a alguien le interesa..

    }

}
