using UnityEngine;
using System.Collections;

public class DeathScript : MonoBehaviour {
	
	
	#region Exposed Fields
	
	/// <summary>
	/// El jugador
	/// </summary>
	public GameObject m_Player;

    public float m_Damage;
	
	#endregion
	
	#region Non-Exposed Fields
	
	/// <summary>
	/// Game Manager para hacer respawn del jugador
	/// </summary>
	private GameObject m_GameManager;
	
	#endregion
	
	/// <summary>
	/// En la función Start hacemos una búsqueda del GameManager
	/// </summary>
	void Start () {

        // TODO 1 - Buscamos un GameObject cuyo tag sea "GameManager"


        //Podriamos tener el nombre del GameManager como constante o arrastrar el GameManager desde el inspector,
        //Haciendo publico m_GameManager (o serializable)

    }
	
	
	/// <summary>
	/// Si algo choca contra nosotros, comprobaremos si es el player
	/// </summary>
	/// <param name="other">
	/// Objeto que ha entrado en el trigger <see cref="Collider"/>
	/// </param>
	void OnTriggerEnter(Collider other)
	{
        // TODO 2 - Comprobamos que el transform del objeto que colisiona, es el player
        if (true) //Se podria hacer mirando su tag
        {
            // TODO 3 - Enviamos un mensaje al GameManager llamando a la funciÃ³n "RespawnPlayer"

            if (GetComponent<AudioSource>())
                GetComponent<AudioSource>().Play();
        }
    }
}
