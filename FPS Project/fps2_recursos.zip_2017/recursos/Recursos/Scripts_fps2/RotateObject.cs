using UnityEngine;
using System.Collections;

// Script que rota el gameObject al cual está asignado
public class RotateObject : MonoBehaviour
{
    /// <summary>
    /// Variable privada que indica si el arma está rotando o no
    /// </summary>
    public bool m_IsRotating = false;

    /// <summary>
    /// Velocidad de rotación
    /// </summary>
    public float m_RotationVelocity = 500.0f;

    /// <summary>
    /// Booleano que indica si la rotación a modificar es local al objeto
    /// </summary>
    public bool m_IsLocal = true;

    // En el update del objeto, únicamente si está permitido rotar, rotaremos el tambor
    void Update()
    {
        // ## TO-DO 1 - Comprobar si se puede rotar, y en caso afirmativo, rotar el tambor en función del tiempo. ##
        // Pista: transform.Rotate


    }

    // Función que setea la variable que indica si el tambor está rotando
    // Puede ser llamada desde fuera
    public void setRotating(bool state)
    {
        m_IsRotating = state;
    }

    //Mensaje Shoot
    void SHOOT(bool state)
    {
        setRotating(state);
    }
}
