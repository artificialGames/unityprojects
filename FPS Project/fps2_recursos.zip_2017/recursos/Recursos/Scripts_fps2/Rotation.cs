using UnityEngine;
using System.Collections;

/// <summary>
/// Este script 
/// </summary>
public class Rotation : MonoBehaviour {
	
	#region Exposed Fields
	
	/// <summary>
	/// Velocidad de rotación
	/// </summary>
	public Vector3 m_RotationSpeed = new Vector3( 0.0f, 75.0f, 0.0f);
	
	#endregion
	
	/// <summary>
	/// En función de la rotación que se pasa desde fuera, se rota el objeto
	/// </summary>
	void Update () {
		transform.Rotate(m_RotationSpeed*Time.deltaTime);
	}
}
