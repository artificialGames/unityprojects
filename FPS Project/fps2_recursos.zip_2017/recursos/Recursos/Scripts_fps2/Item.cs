using UnityEngine;
using System.Collections;


/// <summary>
/// Este componente representa el funcionamiento de un item
/// Hace que el gameObject que lo lleva desaparezca, actualiza el spawnpoint,
/// y activa la puerta del color que se indica en sus atributos publicos
/// </summary>
public class Item : MonoBehaviour {
	
	#region Exposed Fields
	
	/// <summary>
	/// Representa el color de la puerta que se activará cuando
	/// el jugador coja este item
	/// </summary>
	public GameManager.DoorColor m_DoorColorToActivate;
	
	#endregion
	
	#region Non-Exposed Fields
	
	/// <summary>
	/// GameManager presente en la escena. Necesario para activar la puerta que haga falta
	/// </summary>
	private GameObject m_GameManager;
	
	#endregion

	/// <summary>
	///  Inicializaciones necesarias para conseguir el Game Manager
	/// </summary>
	void Start () {
		
		// Buscamos el GameManager
		m_GameManager = GameObject.FindGameObjectWithTag("GameManager");
	}
	
	/// <summary>
	/// En la función de entrada en el trigger, se comprueba que sea
	/// el player el que toca el item.
	/// </summary>
	/// <param name="other">
	/// Objeto que colisiona contra el item (debería ser el player) <see cref="Collider"/>
	/// </param>
	void OnTriggerEnter (Collider other)
	{
		ActivateDoor(other.gameObject);
	}
	
	/// <summary>
	/// Función que activa una puerta determinada
	/// </summary>
	void ActivateDoor(GameObject other)
	{
		// Comprobación del tag del objeto con el que se detecta colisión
		if (other.tag == "Player")
		{

            // TODO 1 - Obtener componente GameManager del GameObject m_GameManager y guardarlo
            // en una variable local
            
            // TODO 2 - Si el componente existe..
            if (true) //Quita esto, solo esta para que compile....
			{
                
                // TODO 3 - llamar directamente a la función ActivateDoor sobre el componente GameManager
                // Como parámetro, habrá que pasarle el m_DoorColorToActivate, y un true, indicando que SI quieres activar la puerta


                // TODO 4 - Crear un nuevo GameObject (indicar que es necesario para crear un nuevo transform)
                // haciendo un new del mismo.
                // Dicho GameObject será el nuevo spawnPoint del player. Habrá que sumarle (0,4,0) a su posición para
                // que al recolocar el player, éste no atraviese el suelo
                // Solución:
                

                // TODO 5 - llamar directamente a la función SetCurrentSpawnPoint sobre el componente GameManager
                // pasando como parámetro la matriz de transformación de nuestro nuevo GameObject
               

                // TODO 6 - Autodestruirse
                //Desaparece el ítem de la escena
                // Pista: Destroy(...);
               
            }
			else
			{
				Debug.LogWarning("No existe componente GameManager dentro del Game Manager");	
			}
		}
	}
}
